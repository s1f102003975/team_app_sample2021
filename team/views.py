from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
from urllib import request
import urllib
import json
import random
from .models import Reply

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'rD1k9sHuaO2YSRdg75TuYSRF'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'


def omikuji_run():

    num = random.randint(0,60)

    if num < 2:
        return "大吉:heart_eyes:"
    elif 2 <= num < 10:
        return "中吉:kissing_smiling_eyes:"
    elif 10 <= num < 20:
        return "小吉:stuck_out_tongue:"
    elif 20 <= num < 40:
        return "吉:open_mouth:"
    elif 40 <= num < 50:
        return "末吉:expressionless:"
    elif 50 <= num < 55:
        return "凶:-1:"
    else:
        return "大凶:shit:"

def index(request):
    positive_replies = Reply.objects.filter(response=Reply.POSITIVE)
    neutral_replies = Reply.objects.filter(response=Reply.NEUTRAL)
    negative_replies = Reply.objects.filter(response=Reply.NEGATIVE)

    context = {
        'positive_replies': positive_replies,
        'neutral_replies': neutral_replies,
        'negative_replies': negative_replies,

    }
    return render(request, 'index.html', context)

def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

@csrf_exempt
def omikuji(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    

    result = {
        'text': '<@{}> {}'.format(user_id, omikuji_run()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)
def janken_run(n):
    p = ["グー", "チョキ", "パー"]
    x = random.randint(0, 2)
    destination = "Tokyo" #default
    if(n == p[0]):
        if(p[x] == p[0]):
            res_janken =  "\nこっちはグー:fist:、あいこです。\n残念ながら天気は返せません。"
        elif(p[x] == p[1]):
            res_janken = "\nこっちはチョキ:v:、あなたの勝ちです"
            return res_janken + "\n" + weather_run(destination) + "\n" + "今日の運勢は" + omikuji_run()
        else:
            res_janken = "\nこっちはパー:raised_hand:、私の勝ちです。残念ながら天気は返せません。"
        return res_janken
    elif(n == p[1]):
        if(p[x] == p[0]):
            res_janken =  "\nこっちはグー:fist:、私の勝ちです。\n残念ながら天気は返せません。"
        elif(p[x] == p[1]):
            res_janken =  "\nこっちはチョキ:v:、あいこです。\n残念ながら天気は返せません。"
        else:
            res_janken =  "\nこっちはパー:raised_hand:、あなたの勝ちです"
            return res_janken + "\n" + weather_run(destination) + "\n" + "今日の運勢は" + omikuji_run()
        return res_janken
    elif(n == p[2]):
        if(p[x] == p[0]):
            res_janken =  "\nこっちはグー:fist:、あなたの勝ちです。\n残念ながら天気は返せません。"
            return res_janken + "\n" + weather_run(destination) + "\n" + "今日の運勢は" + omikuji_run()
        elif(p[x] == p[1]):
            res_janken = "\nこっちはチョキ:v:、私の勝ちです。\n残念ながら天気は返せません。"
        else:
            res_janken = "\nこっちはパー:raised_hand:、あいこです。\n残念ながら天気は返せません。"
        return res_janken
    else:
        return "/rtのあとにグー:fist:、チョキ:v:、パー:raised_hand:のいずれかを入力してください"
    
@csrf_exempt
def rt(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    test = request.POST['text']
    
    result = {
        'text': '<@{}> {}'.format(user_id, janken_run(test)),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

def weather_run(name):
    app_id = "a3450e49fddcd5b1d133931e39d87855"
    URL = "https://api.openweathermap.org/data/2.5/weather?q={0},jp&units=metric&lang=ja&appid={1}".format(name, app_id)

    req = request.Request(URL)
    res = request.urlopen(req)
    data = json.loads(res.read().decode('utf-8'))

    #天気情報
    weather = data["weather"][0]["description"]
    #最高気温
    temp_max = round(data["main"]["temp_max"], 1)
    #最低気温
    temp_min = round(data["main"]["temp_min"], 1)
    #湿度
    humidity = data["main"]["humidity"]

    context = {"天気": weather, "最高気温":str(temp_max) + "度", "最低気温": str(temp_min) + "度",  "湿度": str(humidity) + "%"}
    d = ""
    for k, v in context.items():
        d += "{0}:{1}、".format(k, v)
    return d.rstrip("、")

@csrf_exempt
def weather(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    test = request.POST['text']
    
    result = {
        'text': '<@{}> {}'.format(user_id, weather_run(test)),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)
@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'positive':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
        reply.save()
        response = {
            'text': '<@{}> Great! :smile:'.format(user['id'])
        }
    elif selected_value == 'neutral':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        response = {
            'text': '<@{}> Ok, thank you! :sweat_smile:'.format(user['id'])
        }
    else:
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
        reply.save()
        response = {
            'text': '<@{}> Good luck! :innocent:'.format(user['id'])
        }
    
    post_message(response_url, response)

    return JsonResponse({})

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()